var calculadora = function(){
    // variables globales
    var operando1 = 0;
    var operando2 = 0;
    var pantalla = "";
    var operacion = 0;
    // funciones privadas
    // funciones publicas dentro del return
    return{
        // insertar un numero o el punto
        insertar: function(n){
            if(n == 0 & pantalla == '0'){
                return(0);
            }
            else{
                if(pantalla.substring(0,1) == '0'){ // evitar que el primer numero sea 0
                    pantalla = pantalla.substring(1,pantalla.length);
                }
                pantalla = pantalla+n;
                if(pantalla.length>8){
                    return(pantalla.substring(0,8)); // 9: mostrar solo los primeros 8 digitos
                }
                else{
                    return(pantalla);
                }
            }
        },
        borrar: function(){
            pantalla = "";
        },
        suma: function(){
            operando1 = parseFloat(pantalla);
            pantalla = "";
            operacion = 1;
        },
        resta: function(){
            operando1 = parseFloat(pantalla);
            pantalla = "";
            operacion = 2;
        },
        multiplicacion: function(){
            operando1 = parseFloat(pantalla);
            pantalla = "";
            operacion = 3;
        },
        division: function(){
            operando1 = parseFloat(pantalla);
            pantalla = "";
            operacion = 4;
        },
        igual: function(){
            var R = 0;
            operando2 = parseFloat(pantalla);
            if(operacion == 1){ // opcion 1 = suma
                R = operando1+operando2;
            }
            else if(operacion == 2){ // opcion 2 = resta
                R = operando1-operando2;
            }
            else if(operacion == 3){ // opcion 3 = multiplicacion
                R = operando1*operando2;
            }
            else if(operacion == 4){ // opcion 4 = division
                R = operando1/operando2;
            }

            R = R+"";
            if(R.length>8){
                return(R.substring(0,8)); // 9: mostrar solo los primeros 8 digitos
            }
            operando1 = parseFloat(R)
            pantalla = operando1;
            return(R);
        },
        inv:function(){
			operando1 = parseFloat(pantalla);
			if(operando1 > 0){
				operando1 = 0 - operando1;
			}
			else if(operando1 < 0){
				operando1 = operando1 * -1;
			}
			pantalla = operando1;
			return(operando1);
        },
        raiz:function(){
            var R = 0;
            R = Math.sqrt(parseFloat(pantalla));
            pantalla = R+"";
            pantalla = pantalla.substring(0,8);
            return(pantalla);
        }
    }
}


function initElementos(){
        // 10: objeto calculadora
        var calc = new calculadora();
        var resultado = document.getElementById('display');
        var resetear = document.getElementById("on");
        var masmenos = document.getElementById("sign");
        var raiz = document.getElementById("raiz");
        var divide = document.getElementById("dividido");
        var multiplica = document.getElementById("por");
        var suma = document.getElementById("mas");
        var resta = document.getElementById("menos");
        var igual = document.getElementById("igual");
        var punto = document.getElementById("punto");
        var puntoControl = true;
        var numeros = [];
        // creacion de variables numericas y asignacion de funcion
        for(var i=0;i<10;i++){
            numeros[i] = document.getElementById(i);
        }
        // 5: agregar numeros en pantalla
        numeros[0].onmousedown = function(){
            numeros[0].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(0);
        };
        numeros[0].onmouseup = function(){
            numeros[0].style.transform = 'scale(1)';
        };
        numeros[1].onmousedown = function(){
            numeros[1].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(1);
        };
        numeros[1].onmouseup = function(){
            numeros[1].style.transform = 'scale(1)';
        };
        numeros[2].onmousedown = function(){
            numeros[2].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(2);
        };
        numeros[2].onmouseup = function(){
            numeros[2].style.transform = 'scale(1)';
        };
        numeros[3].onmousedown = function(){
            numeros[3].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(3);
        };
        numeros[3].onmouseup = function(){
            numeros[3].style.transform = 'scale(1)';
        };
        numeros[4].onmousedown = function(){
            numeros[4].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(4);
        };
        numeros[4].onmouseup = function(){
            numeros[4].style.transform = 'scale(1)';
        };
        numeros[5].onmousedown = function(){
            numeros[5].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(5);
        };
        numeros[5].onmouseup = function(){
            numeros[5].style.transform = 'scale(1)';
        };
        numeros[6].onmousedown = function(){
            numeros[6].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(6);
        };
        numeros[6].onmouseup = function(){
            numeros[6].style.transform = 'scale(1)';
        };
        numeros[7].onmousedown = function(){
            numeros[7].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(7);
        };
        numeros[7].onmouseup = function(){
            numeros[7].style.transform = 'scale(1)';
        };
        numeros[8].onmousedown = function(){
            numeros[8].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(8);
        };
        numeros[8].onmouseup = function(){
            numeros[8].style.transform = 'scale(1)';
        };
        numeros[9].onmousedown = function(){
            numeros[9].style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.insertar(9);
        };
        numeros[9].onmouseup = function(){
            numeros[9].style.transform = 'scale(1)';
        };
        divide.onmousedown = function(){
            divide.style.transform = 'scale(0.9)';
        };
        divide.onmouseup = function(){
            divide.style.transform = 'scale(1)';
        };
    
        //Botón Raíz
        raiz.onmousedown = function(){
            raiz.style.transform = 'scale(0.9)';
        };
        raiz.onmouseup = function(){
            raiz.style.transform = 'scale(1)';
        }




        // 7: punto
        punto.onmousedown = function(){
            punto.style.transform = 'scale(0.9)';
            if(puntoControl){
                resultado.innerHTML = calc.insertar('.');
                puntoControl = false;
            }
        };
        punto.onmouseup = function(){
            punto.style.transform = 'scale(1)';
        };
        // operaciones basicas
        suma.onmousedown = function(){
            suma.style.transform = 'scale(0.9)';
            calc.suma(); //Se llama la funcion suma y ésta agrega el número en pantalla al operador 1
            resultado.innerHTML = '0';
            puntoControl = true;
        };
        suma.onmouseup = function(){
            suma.style.transform = 'scale(1)';
        };
        resta.onmousedown = function(){
            resta.style.transform = 'scale(0.9)';
            calc.resta(); //Se llama la funcion resta y ésta agrega el número en pantalla al operador 1
            resultado.innerHTML = '0';
            puntoControl = true;
        };
        resta.onmouseup = function(){
            resta.style.transform = 'scale(1)';
        };
        divide.onmousedown = function(){
            divide.style.transform = 'scale(0.9)';
            calc.division();
            resultado.innerHTML = '0';
            puntoControl = true;
        };
        divide.onmouseup = function(){
            divide.style.transform = 'scale(1)';
        };
        multiplica.onmousedown = function(){
            multiplica.style.transform = 'scale(0.9)';
            calc.multiplicacion();
            resultado.innerHTML = '0';
            puntoControl = true;
        };
        multiplica.onmouseup = function(){
            multiplica.style.transform = 'scale(1)';
        };
        igual.onmousedown = function(){
            igual.style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.igual();
            puntoControl = true;
        };
        igual.onmouseup = function(){
            igual.style.transform = 'scale(1)';
        };
        // 8: invertir +/-
        masmenos.onmousedown = function(){
            masmenos.style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.inv();
        };
        masmenos.onmouseup = function(){
            masmenos.style.transform = 'scale(1)';
        };
        raiz.onmousedown = function(){
            raiz.style.transform = 'scale(0.9)';
            resultado.innerHTML = calc.raiz();
        }
        raiz.onmouseup = function(){
            raiz.style.transform = 'scale(1)';
        }
        // 6: ON/C
        resetear.onmousedown = function(e){
            resetear.style.transform = 'scale(0.9)';
            resultado.innerHTML = '';
            resultado.textContent = resultado.textContent + '0';
            calc.borrar();
        }   
        resetear.onmouseup = function(e){
            resetear.style.transform = 'scale(1)';
        } 
    }   








