var socket = io.connect('http://localhost:8080', { 'forceNew': true });
var data = {
  ciudad:'',
  tipo:'',
  precio:{min:0,max:0}
}
socket.on('registros', function(data) {
  render(data);
});
socket.on('ciudades', function(data) {
  loadSearch(data);
});

socket.on('tipos', function(data){
    cargarTipos(data);
});

// la boton 'ver todos' inicializa el objeto data para que no se filtren los resuldatos y se muestren todos los datos
$('#buscar').click(function(e){
	socket.emit('registrosTodos', data);
});

// muestra solo las ciudades con el nombre seleccionado
$('#ciudad').change(function(){
  data.ciudad = $('#ciudad').val();
});

// muestra solo las ciudades con el tipo seleccionado
$('#tipo').change(function(){
  data.tipo = $('#tipo').val();
});
// muestra solo las ciudades con el rango de precios seleccionado
$('#rangoPrecio').change(function(){
  var r = obtenerRangos();
  data.precio.min = r.min;
  data.precio.max = r.max;
});

// render: muestra en pantalla los datos recibidos del servidor
function render(dt){
	var cards = '';
	for (var i in dt) {
		cards = cards + `<div class="card horizontal">
          <div class="card-image">
            <img src="img/home.jpg">
          </div>
          <div class="card-stacked">
            <div class="card-content">
              <div>
                <b>Direccion: </b><span>${dt[i].Direccion}</spanp>
              </div>
              <div>
                <b>Ciudad: </b><span>${dt[i].Ciudad}</span>
              </div>
              <div>
                <b>Telefono: </b><span>${dt[i].Telefono}</span>
              </div>
              <div>
                <b>Código postal: </b><span>${dt[i].Codigo_Postal}</span>
              </div>
              <div>
                <b>Precio: </b><span>${dt[i].Precio}</span>
              </div>
              <div>
                <b>Tipo: </b><span>${dt[i].Tipo}</span>
              </div>
            </div>
            <div class="card-action right-align">
              <a href="#">Ver más</a>
            </div>
          </div>
        </div>`;
	}
	$('.lista').html(cards);
}
// loadSearch: carga los options en los elementos de la busqueda avanzada
function loadSearch(dt){
	var listCities = '';
	for(var i in dt) {
    	if(dt[i] != null){
			listCities = listCities + `<option value="${dt[i]}">${dt[i]}</option>`;
		}
	}
	$('#ciudad').append(listCities);
}


function cargarTipos(dt){
     var listaTipos = '';
    for(var i in dt) {
    	if(dt[i] != null){
			listaTipos = listaTipos + `<option value="${dt[i]}">${dt[i]}</option>`;
		}
	}
	$('#tipo').append(listaTipos); 
}
function obtenerRangos(){
  var rango = {
    min:0,
    max:0
  };
  var r,rMin='',rMax='';
  r = $('#rangoPrecio').val();
  for (var i = 0; i < r.indexOf(';'); i++) {rMin = rMin + r[i];}
  for (var i = (r.indexOf(';')+1); i < r.length; i++) {rMax = rMax + r[i];}
  rango.min = parseInt(rMin);
  rango.max = parseInt(rMax);
  return(rango);
}


// eventos al inicio de la ejecucion
socket.emit('obtenerCiudades');
socket.emit('obtenerTipos');
