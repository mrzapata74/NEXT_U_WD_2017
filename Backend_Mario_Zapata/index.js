var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 8080;
app.use(express.static('public'));

// modulo que se encarga de gestionar todos los datos de la base de datos
var db = require('./database/loadData.js');

app.get('/home', function(req, res) {
  res.sendFile('public/index.html' , { root : __dirname});
});

io.on('connection', function(socket) {
  console.log('Alguien se ha conectado con Sockets');

  // obtener todos los registros
  socket.on('registrosTodos', function(data) {
    io.sockets.emit('registros', db.getAll(data));
  });
  // obtener las ciudades en los registros
  socket.on('obtenerCiudades', function() {
    io.sockets.emit('ciudades', db.getCities());
  });
    
   socket.on('obtenerTipos', function() {
   io.sockets.emit('tipos', db.getTipos());
  });
}); 





server.listen(port, () =>{
    console.log("Servidor de Tienda Ejecutándose ");
})