var fs = require('fs');
var dt;
var ciudades = [];
var tipos = [];

// --- Lectura del archivo JSON y validación al haber error
fs.readFile('./public/data.json', 'utf-8', (err, data) => {
	  if(err) {
	    console.log('error: ', err);
	  }
	  else {
	    dt = JSON.parse(data); //Carga los datos en formato JSON en la variable dt
	  }
});
// --- borrar elementos duplicados de un array
Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});
//______________________________________

function obtenerTodos(data){
    var _dt = dt;
    if(data.ciudad == '' && data.tipo == '' && data.precio.min == 0 && data.precio.max == 0){
        return dt;
    }
    else{
        var tmp = [];
        if(data.ciudad.length > 0){
            for(var i = 0;i < _dt.length;i++) {
                if (data.ciudad.localeCompare(_dt[i].Ciudad) == 0){
                    tmp.push(_dt[i]);
                }
            }
            _dt = tmp;
            tmp = [];
        }
        if(data.tipo.length > 0){
            for(var i = 0;i < _dt.length;i++) {
                if (data.tipo.localeCompare(_dt[i].Tipo) == 0){
                    tmp.push(_dt[i]);
                }
            }
            _dt = tmp;
            tmp = [];
        }
        if(data.precio.min != 0 && data.precio.max != 0){
            var p;
            for(var i = 0;i < _dt.length;i++) {
                p = '';
                for(var x = 1;x<_dt[i].Precio.length;x++){
                    if(_dt[i].Precio[x] != ','){
                        p=p+_dt[i].Precio[x];
                    }
                }
                p = parseFloat(p);
                if(p > data.precio.min && p < data.precio.max){
                    tmp.push(_dt[i]);
                }
            }
            _dt = tmp;
        }
    }
    return(_dt);
}

//______________________________________

function obtenerCiudades(){
    var _ciudades = [];
    for (var i in dt) {
        _ciudades.push(dt[i].Ciudad);
    }
    ciudades = _ciudades.unique();
	 return(ciudades.sort());
}

//__________________________________

function obtenerTipos(){
    var _tipos = [];
    for (var i in dt) {
        _tipos.push(dt[i].Tipo);
    }
    tipos = _tipos.unique();
     return(tipos.sort());   
}






// exportar modulos
exports.getAll=obtenerTodos;
exports.getCities=obtenerCiudades;
exports.getTipos=obtenerTipos;